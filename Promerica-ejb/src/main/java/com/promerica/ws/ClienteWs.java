/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ws;

import com.promerica.modelos.Cliente;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author kirio
 */
@WebService
public interface ClienteWs {
    
    @WebMethod(operationName = "todosLosClientes")
    public List<Cliente> clientes();
    @WebMethod(operationName = "buscarPorId")
    public Cliente buscarCliente(Long id);
}
