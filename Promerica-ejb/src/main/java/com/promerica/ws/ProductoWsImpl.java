/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ws;

import com.promerica.dao.ProductoDao;
import com.promerica.modelos.Producto;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

/**
 *
 * @author kirio
 */
@Stateless
@WebService(endpointInterface = "com.promerica.ws.ProductoWs")
public class ProductoWsImpl implements ProductoWs {

    @EJB
    private ProductoDao dao;
    
    @Override
    public List<Producto> productos() {
        return dao.productos();
    }

    @Override
    public Producto buscarProductos(Long id) {
       return dao.buscarProductos(id);
    }
    
}
