/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ws;

import com.promerica.modelos.Orden;
import java.util.List;
import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author kirio
 */

@WebService
public interface OrdenWs {
    @WebMethod(operationName="guardarOrden")
    public void guardar(Orden orden);
    @WebMethod(operationName="actulizarOrden")
    public void actualizar(Orden orden);
    @WebMethod(operationName="todasLasOrdenes")
    public List<Orden> ordenes();
    @WebMethod(operationName="buscarOrden")
    public Orden buscar(Long idCliente);
    @WebMethod(operationName="eliminarOrden")
    public void eliminar(Long idCliente);
}
