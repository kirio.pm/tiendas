/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ws;

import com.promerica.modelos.Producto;
import java.util.List;
import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author kirio
 */

@WebService
public interface ProductoWs {
    
    @WebMethod(operationName = "todosLosProductos")
    public List<Producto> productos();
    
    @WebMethod(operationName = "buscarPorId")
    public Producto buscarProductos(Long id);
}
