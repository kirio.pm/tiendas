/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ws;

import com.promerica.dao.OrdenesDao;
import com.promerica.modelos.Orden;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

/**
 *
 * @author kirio
 */
@Stateless
@WebService(endpointInterface = "com.promerica.ws.OrdenWs")
public class OrdenWsImpl implements OrdenWs{

    @EJB
    private OrdenesDao dao;
    
    @Override
    public void guardar(Orden orden) {
        dao.guardar(orden);
    }

    @Override
    public void actualizar(Orden orden) {
        dao.actualizar(orden);
    }

    @Override
    public List<Orden> ordenes() {
        return dao.ordenes();
    }

    @Override
    public Orden buscar(Long idCliente) {
        return dao.buscar(idCliente);
    }

    @Override
    public void eliminar(Long idCliente) {
        dao.eliminar(idCliente);
    }
    
}
