/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ws;

import com.promerica.dao.ClienteDao;
import com.promerica.modelos.Cliente;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

/**
 *
 * @author kirio
 */
@Stateless
@WebService(endpointInterface = "com.promerica.ws.ClienteWs")
public class ClienteWsImpl implements ClienteWs{
    
    @EJB
    private ClienteDao dao;

    @Override
    public List<Cliente> clientes() {
        return dao.clientes();
    }

    @Override
    public Cliente buscarCliente(Long id) {
        return dao.buscarCliente(id);
    }
    
}
