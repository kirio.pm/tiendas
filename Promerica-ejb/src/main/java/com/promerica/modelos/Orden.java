/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.modelos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

/**
 *
 * @author kirio
 */
@Data
@Entity
@Table(name = "Orden")
@NamedQueries({
    @NamedQuery(name = "Orden.findAll", query = "SELECT o FROM Orden o")
    , @NamedQuery(name = "Orden.findByIdOrden", query = "SELECT o FROM Orden o WHERE o.id = :id")
    , @NamedQuery(name = "Orden.betweenOf", query = "SELECT o FROM Orden o WHERE o.fecha BETWEEN :inicio AND :fin")
    , @NamedQuery(name = "Orden.findByCodigo", query = "SELECT o FROM Orden o WHERE o.codigo = :codigo")})
public class Orden implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_orden" )
    private Long id;
    private String codigo;
    private Integer cantidad;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    private Cliente cliente;
    
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "ordenes_productos",
            joinColumns = {
                @JoinColumn(name = "id_orden")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_producto")}) 
    private List<Producto> productos;
    
}
