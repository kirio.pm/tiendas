/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.dao;

import com.promerica.modelos.Cliente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author kirio
 */
@Local
public interface ClienteDao {
    public List<Cliente> clientes();
    public Cliente buscarCliente(Long id);
    
}
