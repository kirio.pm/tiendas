/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.dao;

import com.promerica.modelos.Producto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kirio
 */
@Stateless
public class ProductoDaoImpl implements ProductoDao{
    @PersistenceContext(unitName = "tiendasPU")
    private EntityManager em;
    
    @Override
    public List<Producto> productos() {
        return em.createNamedQuery("Producto.findAll").getResultList();
    }

    @Override
    public Producto buscarProductos(Long id) {
        return em.find(Producto.class, id);
    }
    
}
