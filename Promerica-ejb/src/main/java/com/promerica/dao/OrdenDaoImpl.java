/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.dao;

import com.promerica.modelos.Orden;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kirio
 */
@Stateless
public class OrdenDaoImpl implements OrdenesDao {

    @PersistenceContext(unitName = "tiendasPU")
    private EntityManager em;
    
    @Override
    public void guardar(Orden orden) {        
       em.merge(orden);
    }

    @Override
    public void actualizar(Orden orden) {
        em.persist(orden);
    }

    @Override
    public List<Orden> ordenes() {
        return em.createNamedQuery("Orden.findAll").getResultList();
    }

    @Override
    public Orden buscar(Long idCliente) {
        return em.find(Orden.class, idCliente);
    }

    @Override
    public void eliminar(Long idCliente) {        
        em.remove(buscar(idCliente));
    }
    
}
