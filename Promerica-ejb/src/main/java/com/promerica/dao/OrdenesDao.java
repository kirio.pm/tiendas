/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.dao;

import com.promerica.modelos.Orden;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author kirio
 */
@Local
public interface OrdenesDao {
    public void guardar(Orden orden);
    public void actualizar(Orden orden);
    public List<Orden> ordenes();
    public Orden buscar(Long idCliente);
    public void eliminar(Long idCliente);
}
