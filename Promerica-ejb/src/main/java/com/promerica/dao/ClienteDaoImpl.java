/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.dao;

import com.promerica.modelos.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kirio
 */
@Stateless
public class ClienteDaoImpl implements ClienteDao{

    @PersistenceContext(unitName = "tiendasPU")
    private EntityManager em;
    
    @Override
    public List<Cliente> clientes() {
        return em.createNamedQuery("Cliente.findAll").getResultList();
    }

    @Override
    public Cliente buscarCliente(Long id) {
        return em.find(Cliente.class, id);
    }
    
}
