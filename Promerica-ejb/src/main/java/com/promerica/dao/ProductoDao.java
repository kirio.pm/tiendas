/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.dao;

import com.promerica.modelos.Producto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author kirio
 */
@Local
public interface ProductoDao {
     public List<Producto> productos();
     public Producto buscarProductos(Long id);     
}
