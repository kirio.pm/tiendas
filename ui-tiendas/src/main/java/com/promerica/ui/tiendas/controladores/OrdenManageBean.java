/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.controladores;

import com.promerica.ui.tiendas.dto.Cliente;
import com.promerica.ui.tiendas.dto.Orden;
import com.promerica.ui.tiendas.dto.Producto;
import com.promerica.ui.tiendas.wscliente.ClienteClient;
import com.promerica.ui.tiendas.wscliente.OrdenClient;
import com.promerica.ui.tiendas.wscliente.ProductoClient;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
/**
 *
 * @author kirio
 */
@Named(value = "ordenManagedBean")
@ViewScoped
public class OrdenManageBean {
    
    @EJB
    private OrdenClient ord;    
    @EJB
    private ProductoClient prd;
    @EJB
    private ClienteClient cls;
    
    private Orden orden;
    private List<Orden> ordenes;
    private List<Producto> productos;
    private Cliente cliente;
    private List<Cliente> clientes;
    
    public OrdenManageBean(){
       orden= new Orden() ;
       ordenes = new ArrayList<>();
       productos= new ArrayList<>();;
       cliente=new Cliente();
       clientes= new ArrayList<>();
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public List<Orden> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<Orden> ordenes) {
        this.ordenes = ordenes;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
    
    public List<Cliente> todosClientes() {
        return clientes=cls.todos();
    }
    
    public List<Orden> todosOrdenes() {
        return ordenes=ord.todas();
    }
    
    public List<Producto> todosProductos() {
        return prd.todos();
    }
   
    public void guardarOrden() {
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {            
                ord.ingresar(orden);
                requestContext.execute("PF('dlg_agregar').hide();");
                orden= new Orden();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "ORDEN GUARDADA "));           
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "orden NO SE PUDO GUARDAR "));
        }
    }

    public void actualizarOrden() {
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {           
                ord.editar(orden);
                requestContext.execute("PF('dlg_editar').hide();");
                orden= new Orden();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "ORDEN ACTUALIZADA "));
            
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "ORDEN NO SE PUDO ACTUALIZAR"));
        }
    }

    public void eliminarOrden() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {           
                ord.eliminar(orden.getId());
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "ORDEN ELIMINADA "));
            
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "ORDEN NO SE HA PODIDO ELIMINAR"));
        }
    }

    public void limpiarDialogo() {
        orden= new Orden();
    }
    
    
}
