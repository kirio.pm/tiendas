/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.wscliente;

import com.promerica.ui.tiendas.dto.Producto;
import com.promerica.ws.ProductoWsImplService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author kirio
 */
@Stateless
public class ProductoClientImpl implements ProductoClient{

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/ProductoWsImplService/ProductoWsImpl.wsdl")
    private ProductoWsImplService service;

    @Override
    public List<Producto> todos() {
        List<Producto> productos= new ArrayList<>();
        try { // Call Web Service Operation
            com.promerica.ws.ProductoWs port = service.getProductoWsImplPort();
            // TODO process result here
            java.util.List<com.promerica.ws.Producto> result = port.todosLosProductos();
            
            for(com.promerica.ws.Producto p:result){
                Producto producto= new Producto();
                producto.setId(p.getId());
                producto.setCodigo(p.getCodigo());
                producto.setDescripcion(p.getDescripcion());
                producto.setPrecio(p.getPrecio());
                productos.add(producto);
            }
            return productos;
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Producto porId(Long id) {
        Producto p = new Producto();
        try { // Call Web Service Operation
            com.promerica.ws.ProductoWs port = service.getProductoWsImplPort();
            // TODO initialize WS operation arguments here
            java.lang.Long arg0 = Long.valueOf(0L);
            // TODO process result here
            com.promerica.ws.Producto result = port.buscarPorId(arg0);
            p.setId(result.getId());
            p.setNombre(result.getNombre());
            return p;
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.   
        
    }
    
}
