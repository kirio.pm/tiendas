/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.wscliente;

import com.promerica.ui.tiendas.dto.Orden;
import com.promerica.ws.OrdenWsImplService;

import java.util.List;
import javax.ejb.Stateless;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author kirio
 */
@Stateless
public class OrdenClientImpl implements OrdenClient{

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/OrdenWsImplService/OrdenWsImpl.wsdl")
    private OrdenWsImplService service;
  
    @Override
    public void ingresar(Orden orden) {
        try { // Call Web Service Operation
            com.promerica.ws.OrdenWs port = service.getOrdenWsImplPort();
            // TODO initialize WS operation arguments here
            com.promerica.ws.Orden arg0 = new com.promerica.ws.Orden();            
            
            port.guardarOrden(arg0);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

       
    }

    @Override
    public void editar(Orden orden) {
        
        try { // Call Web Service Operation
            com.promerica.ws.OrdenWs port = service.getOrdenWsImplPort();
            // TODO initialize WS operation arguments here
            com.promerica.ws.Orden arg0 = new com.promerica.ws.Orden();
            port.actulizarOrden(arg0);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

    }

    @Override
    public void eliminar(Long id) {
        
        try { // Call Web Service Operation
            com.promerica.ws.OrdenWs port = service.getOrdenWsImplPort();
            // TODO initialize WS operation arguments here
            java.lang.Long arg0 = Long.valueOf(0L);
            port.eliminarOrden(arg0);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

    }

    @Override
    public Orden buscarPorId(Long id) {
        
        try { // Call Web Service Operation
            com.promerica.ws.OrdenWs port = service.getOrdenWsImplPort();
            // TODO initialize WS operation arguments here
            java.lang.Long arg0 = Long.valueOf(0L);
            // TODO process result here
            com.promerica.ws.Orden result = port.buscarOrden(arg0);
            System.out.println("Result = "+result);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }
        return null;

    }

    @Override
    public List<Orden> todas() {
        
        try { // Call Web Service Operation
            com.promerica.ws.OrdenWs port = service.getOrdenWsImplPort();
            // TODO process result here
            java.util.List<com.promerica.ws.Orden> result = port.todasLasOrdenes();
            System.out.println("Result = "+result);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }
        return null;
    }
    
}
