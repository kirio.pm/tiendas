/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.convertidores;


import com.promerica.ui.tiendas.dto.Cliente;
import com.promerica.ui.tiendas.wscliente.ClienteClient;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author kirio
 */
@FacesConverter("clienteConverter")
public class ClienteConverter implements Converter {

    @EJB
    private ClienteClient cls;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            System.out.println(value);
            try {

                Cliente cliente = cls.porId(Long.valueOf(value));
                return cliente;

            } catch (Exception e) {
                FacesMessage msg = new FacesMessage("Error!", "cliente no válido");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ConverterException(msg);

            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return String.valueOf(((Cliente) object).getId());
        } else {
            return null;
        }
    }
    
}
