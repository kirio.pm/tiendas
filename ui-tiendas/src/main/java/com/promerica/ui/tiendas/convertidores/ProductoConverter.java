/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.convertidores;

import com.promerica.ui.tiendas.dto.Producto;
import com.promerica.ui.tiendas.wscliente.ProductoClient;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author kirio
 */
@FacesConverter("productoConverter")
public class ProductoConverter implements Converter {
    @EJB
    private ProductoClient prd;
   
 @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            System.out.println(value);
            try {

                Producto producto = prd.porId(Long.valueOf(value));
                return producto;

            } catch (Exception e) {
                FacesMessage msg = new FacesMessage("Error!", "producto no válido");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ConverterException(msg);

            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return String.valueOf(((Producto) object).getId());
        } else {
            return null;
        }
    }
    
}
