/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.wscliente;

import com.promerica.ui.tiendas.dto.Producto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author kirio
 */
@Local
public interface ProductoClient {
    
    public List<Producto> todos();
    public Producto porId(Long id);
    
}
