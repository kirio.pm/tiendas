/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.wscliente;

import com.promerica.ui.tiendas.dto.Orden;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author kirio
 */
@Local
public interface OrdenClient {
    public void ingresar(Orden orden);
    public void editar(Orden orden);
    public void eliminar(Long id);
    public Orden buscarPorId(Long id);
    public List<Orden> todas();
}
