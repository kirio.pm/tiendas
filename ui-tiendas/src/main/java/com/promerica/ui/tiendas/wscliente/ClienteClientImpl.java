/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promerica.ui.tiendas.wscliente;

import com.promerica.ui.tiendas.dto.Cliente;
import com.promerica.ws.ClienteWsImplService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author kirio
 */
@Stateless
public class ClienteClientImpl implements ClienteClient{

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/ClienteWsImplService/ClienteWsImpl.wsdl")
    private ClienteWsImplService service;

    @Override
    public List<Cliente> todos() {
        List<Cliente> clientes=new ArrayList<>();
        try { // Call Web Service Operation
            com.promerica.ws.ClienteWs port = service.getClienteWsImplPort();
            // TODO process result here
            java.util.List<com.promerica.ws.Cliente> result = port.todosLosClientes();
            
            for(com.promerica.ws.Cliente c: result){
                Cliente cl= new Cliente();
                cl.setId(c.getId());
                cl.setCodigo(c.getCodigo());
                cl.setApellido(c.getApellido());
                cl.setNombre(c.getNombre());
                clientes.add(cl);
            }
            return clientes;
            
        } catch (Exception ex) {
         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        
    }

    @Override
    public Cliente porId(Long id) {
        Cliente cl = new Cliente();
        
        try { // Call Web Service Operation
            com.promerica.ws.ClienteWs port = service.getClienteWsImplPort();
            // TODO initialize WS operation arguments here
            java.lang.Long arg0 = Long.valueOf(0L);
            // TODO process result here
            com.promerica.ws.Cliente result = port.buscarPorId(arg0);
            
            cl.setId(result.getId());
            cl.setNombre(result.getNombre());
            return cl;
        } catch (Exception ex) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        
    }
    
}
