--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.16
-- Dumped by pg_dump version 12.3

-- Started on 2021-09-02 22:36:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 186 (class 1259 OID 43829)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    id_cliente integer NOT NULL,
    apellido character varying(255),
    codigo character varying(255),
    fechacreacion timestamp without time zone,
    nombre character varying(255)
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 43827)
-- Name: cliente_id_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_cliente_seq OWNER TO postgres;

--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 185
-- Name: cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_id_cliente_seq OWNED BY public.cliente.id_cliente;


--
-- TOC entry 190 (class 1259 OID 43851)
-- Name: orden; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orden (
    id_orden integer NOT NULL,
    cantidad integer,
    codigo character varying(255),
    fecha timestamp without time zone,
    id_cliente bigint
);


ALTER TABLE public.orden OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 43849)
-- Name: orden_id_orden_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orden_id_orden_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orden_id_orden_seq OWNER TO postgres;

--
-- TOC entry 2163 (class 0 OID 0)
-- Dependencies: 189
-- Name: orden_id_orden_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orden_id_orden_seq OWNED BY public.orden.id_orden;


--
-- TOC entry 191 (class 1259 OID 43857)
-- Name: ordenes_productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ordenes_productos (
    id_producto bigint NOT NULL,
    id_orden bigint NOT NULL
);


ALTER TABLE public.ordenes_productos OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 43840)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.producto (
    id_producto integer NOT NULL,
    codigo character varying(255),
    descripcion character varying(255),
    nombre character varying(255),
    precio double precision
);


ALTER TABLE public.producto OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 43838)
-- Name: producto_id_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.producto_id_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.producto_id_producto_seq OWNER TO postgres;

--
-- TOC entry 2164 (class 0 OID 0)
-- Dependencies: 187
-- Name: producto_id_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.producto_id_producto_seq OWNED BY public.producto.id_producto;


--
-- TOC entry 2019 (class 2604 OID 43832)
-- Name: cliente id_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id_cliente SET DEFAULT nextval('public.cliente_id_cliente_seq'::regclass);


--
-- TOC entry 2021 (class 2604 OID 43854)
-- Name: orden id_orden; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orden ALTER COLUMN id_orden SET DEFAULT nextval('public.orden_id_orden_seq'::regclass);


--
-- TOC entry 2020 (class 2604 OID 43843)
-- Name: producto id_producto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto ALTER COLUMN id_producto SET DEFAULT nextval('public.producto_id_producto_seq'::regclass);


--
-- TOC entry 2151 (class 0 OID 43829)
-- Dependencies: 186
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente (id_cliente, apellido, codigo, fechacreacion, nombre) FROM stdin;
\.


--
-- TOC entry 2155 (class 0 OID 43851)
-- Dependencies: 190
-- Data for Name: orden; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orden (id_orden, cantidad, codigo, fecha, id_cliente) FROM stdin;
\.


--
-- TOC entry 2156 (class 0 OID 43857)
-- Dependencies: 191
-- Data for Name: ordenes_productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ordenes_productos (id_producto, id_orden) FROM stdin;
\.


--
-- TOC entry 2153 (class 0 OID 43840)
-- Dependencies: 188
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.producto (id_producto, codigo, descripcion, nombre, precio) FROM stdin;
\.


--
-- TOC entry 2165 (class 0 OID 0)
-- Dependencies: 185
-- Name: cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_id_cliente_seq', 1, false);


--
-- TOC entry 2166 (class 0 OID 0)
-- Dependencies: 189
-- Name: orden_id_orden_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orden_id_orden_seq', 1, false);


--
-- TOC entry 2167 (class 0 OID 0)
-- Dependencies: 187
-- Name: producto_id_producto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.producto_id_producto_seq', 1, false);


--
-- TOC entry 2023 (class 2606 OID 43837)
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id_cliente);


--
-- TOC entry 2027 (class 2606 OID 43856)
-- Name: orden orden_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orden
    ADD CONSTRAINT orden_pkey PRIMARY KEY (id_orden);


--
-- TOC entry 2029 (class 2606 OID 43861)
-- Name: ordenes_productos ordenes_productos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordenes_productos
    ADD CONSTRAINT ordenes_productos_pkey PRIMARY KEY (id_producto, id_orden);


--
-- TOC entry 2025 (class 2606 OID 43848)
-- Name: producto producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id_producto);


--
-- TOC entry 2030 (class 2606 OID 43862)
-- Name: orden fk_orden_id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orden
    ADD CONSTRAINT fk_orden_id_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id_cliente);


--
-- TOC entry 2032 (class 2606 OID 43872)
-- Name: ordenes_productos fk_ordenes_productos_id_orden; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordenes_productos
    ADD CONSTRAINT fk_ordenes_productos_id_orden FOREIGN KEY (id_orden) REFERENCES public.orden(id_orden);


--
-- TOC entry 2031 (class 2606 OID 43867)
-- Name: ordenes_productos fk_ordenes_productos_id_producto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordenes_productos
    ADD CONSTRAINT fk_ordenes_productos_id_producto FOREIGN KEY (id_producto) REFERENCES public.producto(id_producto);


-- Completed on 2021-09-02 22:36:30

--
-- PostgreSQL database dump complete
--

